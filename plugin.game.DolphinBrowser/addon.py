import sys
import os
import xbmc
import xbmcgui
import xbmcplugin
import xbmcaddon
import urlparse
import urllib2
#load settings
my_addon = xbmcaddon.Addon()
isoDir = my_addon.getSetting('link')
#load url
addon_handle = int(sys.argv[1])
base_url = sys.argv[0]
args = urlparse.parse_qs(sys.argv[2][1:])
#route URL
def route():
    params = dict(args)
    if params:
        if params['action'][0] == 'setting':
            setIt()
        if params['action'][0] == 'runIt':
            runIt(params['GameName'][0])
    else:
        listGames()

#list all the games in isoDir and link them
def listGames():
    xbmcplugin.setContent(addon_handle, 'movies')
    #settings:::
    url='plugin://script.games.DolphinBrowser/?action=setting'
    li = xbmcgui.ListItem("Dolphin-Settings", iconImage='DefaultAddonService.png')
    xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li)
    #games::: 
    for f in os.listdir(isoDir):
        if f.split('.')[-1]=='iso':
            url='plugin://script.games.DolphinBrowser/?action=runIt&GameName='+f
            li = xbmcgui.ListItem(f, iconImage='DefaultAddonGame.png')
            xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li)
    xbmcplugin.endOfDirectory(addon_handle)

#execute Dolphin emulator with a game 
def runIt(name):
    os.system("dolphin-emu -b --exec '"+isoDir+name+"'") 

#execute Dolphin for settings
def setIt():
    os.system("dolphin-emu")

route()
