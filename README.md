# A Simple Kodi Frontend for the Dolphin Emulator

A small Kodi Plugin to browse Gamecube- (and in theory Wii-) games an start them in Dolphin

## Installation

### requirement
*  Linux System (sadly Dolphin dosn't run on a Rsapberry Pi)
*  [Kodi](https://kodi.tv)
*  [Dolphin emulator](https://dolphin-emu.org/)

### Installation
**First way:** just install the zip File in Kodi ([How-To](https://kodi.wiki/view/HOW-TO:Install_add-ons_from_zip_files))

**Second way:** Kopy the plugin.game.DolphinBrowser folder to ~/.kodi/addons/
### setting
**addon settings:**

Go to Configure and set "link" to the folder where your .iso files are

**Dolphin Settings:**
now you go to the addon, there is an listitem "Dolphin-Settings" this links to the normal Dolphin menu starts. 
Start the Graphic-settings and there select "use Fullscreen" and "Keep Window on Top"


Now you can Play your games (maybe configureate your Controllers...)

